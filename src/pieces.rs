use bevy::prelude::*;
use rand::prelude::*;

use crate::{block::{BLOCK_INSET, Block, BlockSize}, game_event::GameEvent, position::Position};

#[derive(Clone, Debug, Default)]
pub(crate) struct Piece {
    blocks: Vec<Position>,
    size: i32,
}

/// Position of piece on playing field. (0, 0) is upper left and x increases to
/// the right and y increases downwards.
#[derive(Clone, Copy, Debug)]
pub(crate) struct PiecePosition(pub Position);

/// A component to indicate that some piece being movable (by a gamepad for
/// example). Keeps track of the piece's size.
#[derive(Debug)]
pub(crate) struct ActivePiece {
    pub piece_size: i32,
}

#[derive(Clone, Debug)]
struct Materials {
    middle_blue: Handle<ColorMaterial>,
    blurple: Handle<ColorMaterial>,
    quince_jelly: Handle<ColorMaterial>,
    sun_flower: Handle<ColorMaterial>,
    seabrook: Handle<ColorMaterial>,
    protoss_pylon: Handle<ColorMaterial>,
    light_greenish_blue: Handle<ColorMaterial>,
    exodus_fruit: Handle<ColorMaterial>,
    chi_gong: Handle<ColorMaterial>,
}

pub(crate) fn create_pieces(
    commands: &mut Commands,
    mut materials: ResMut<Assets<ColorMaterial>>,
) {
    let middle_blue = Color::hex("7ed6df").unwrap();
    let blurple = Color::hex("4834d4").unwrap();
    let quince_jelly = Color::hex("f0932b").unwrap();
    let sun_flower = Color::hex("f1c40f").unwrap();
    let seabrook = Color::hex("487eb0").unwrap();
    let protoss_pylon = Color::hex("00a8ff").unwrap();
    let light_greenish_blue = Color::hex("55efc4").unwrap();
    let exodus_fruit = Color::hex("6c5ce7").unwrap();
    let chi_gong = Color::hex("d63031").unwrap();

    let materials = Materials {
        middle_blue: materials.add(middle_blue.into()),
        blurple: materials.add(blurple.into()),
        quince_jelly: materials.add(quince_jelly.into()),
        sun_flower: materials.add(sun_flower.into()),
        seabrook: materials.add(seabrook.into()),
        protoss_pylon: materials.add(protoss_pylon.into()),
        light_greenish_blue: materials.add(light_greenish_blue.into()),
        exodus_fruit: materials.add(exodus_fruit.into()),
        chi_gong: materials.add(chi_gong.into()),
    };

    commands.insert_resource(materials.clone());

    let i = Piece {
        blocks: vec![
            Position::new(0, 1),
            Position::new(1, 1),
            Position::new(2, 1),
            Position::new(3, 1),
        ],
        size: 4,
    };
    let j = Piece {
        blocks: vec![
            Position::new(0, 0),
            Position::new(0, 1),
            Position::new(1, 1),
            Position::new(2, 1),
        ],
        size: 3,
    };
    let l = Piece {
        blocks: vec![
            Position::new(0, 2),
            Position::new(0, 1),
            Position::new(1, 1),
            Position::new(2, 1),
        ],
        size: 3,
    };
    let o = Piece {
        blocks: vec![
            Position::new(0, 0),
            Position::new(0, 1),
            Position::new(1, 0),
            Position::new(1, 1),
        ],
        size: 2,
    };
    let s = Piece {
        blocks: vec![
            Position::new(1, 0),
            Position::new(2, 0),
            Position::new(0, 1),
            Position::new(1, 1),
        ],
        size: 3,
    };
    let t = Piece {
        blocks: vec![
            Position::new(1, 0),
            Position::new(0, 1),
            Position::new(1, 1),
            Position::new(2, 1),
        ],
        size: 3,
    };
    let z = Piece {
        blocks: vec![
            Position::new(0, 0),
            Position::new(1, 0),
            Position::new(1, 1),
            Position::new(2, 1),
        ],
        size: 3,
    };
    let pieces = vec![
        (i, materials.middle_blue.clone(), middle_blue),
        (j, materials.blurple.clone(), blurple),
        (l, materials.quince_jelly.clone(), quince_jelly),
        (o, materials.sun_flower.clone(), sun_flower),
        (
            s,
            materials.light_greenish_blue.clone(),
            light_greenish_blue,
        ),
        (t, materials.exodus_fruit.clone(), exodus_fruit),
        (z, materials.chi_gong.clone(), chi_gong),
    ];
    commands.insert_resource(pieces);
}

pub(crate) fn spawn_piece_event_handler(
    commands: &mut Commands,
    pieces: Res<Vec<(Piece, Handle<ColorMaterial>, Color)>>,
    block_size: Res<BlockSize>,
    mut event_reader: Local<EventReader<GameEvent>>,
    game_events: Res<Events<GameEvent>>,
) {
    for event in event_reader.iter(&game_events) {
        match &event {
            GameEvent::SpawnNewPiece => {
                spawn_piece2(commands, &pieces, block_size.0)
            }
            _ => {}
        }
    }
}

/// This one works.
fn spawn_piece(
    commands: &mut Commands,
    pieces: &[(Piece, Handle<ColorMaterial>, Color)],
    block_size: f32,
) {
    let mut rng = rand::thread_rng();
    let (piece, material, color) = &pieces[rng.gen_range(0..pieces.len())];

    commands
        .spawn((
            ActivePiece {
                piece_size: piece.size,
            },
            Transform::from_translation(Vec3::new(100.0, 200.0, 0.0)),
            GlobalTransform::default(),
            PiecePosition(Position { x: 0, y: 0 }),
        )).with_children(|parent| {
            for &block_pos in piece.blocks.iter() {
                parent.spawn(SpriteBundle {
                    sprite: Sprite::new(Vec2::new(
                        block_size - BLOCK_INSET as f32,
                        block_size - BLOCK_INSET as f32,
                    )),
                    // transform: Transform::from_translation(Vec3::new(150.0, 0.0, 0.0)),
                    material: material.clone(),
                    ..Default::default()
                })
                    .with(Block { position: block_pos })
                    .with(*color);
            }
        });
}

/// This one causes blocks to appear at (0, 0) for a frame.
fn spawn_piece2(
    commands: &mut Commands,
    pieces: &[(Piece, Handle<ColorMaterial>, Color)],
    block_size: f32,
) {
    let mut rng = rand::thread_rng();
    let (piece, material, color) = &pieces[rng.gen_range(0..pieces.len())];

    let parent = commands
        .spawn((
            ActivePiece {
                piece_size: piece.size,
            },
            Transform::from_translation(Vec3::new(100.0, 200.0, 0.0)),
            GlobalTransform::default(),
            PiecePosition(Position { x: 0, y: 0 }),
        )).current_entity().unwrap();

    for &block_pos in piece.blocks.iter() {

        let block_entity = commands
            .spawn(SpriteBundle {
                sprite: Sprite::new(Vec2::new(
                    block_size - BLOCK_INSET as f32,
                    block_size - BLOCK_INSET as f32,
                )),
                transform: Transform::from_translation(Vec3::new(150.0, 0.0, 0.0)),
                material: material.clone(),
                ..Default::default()
            })
            .with(Block { position: block_pos })
            .with(*color)
            .current_entity()
            .unwrap();

        commands.set_current_entity(block_entity);
        commands.with(Parent(parent));
    }
}
