use std::collections::HashMap;

use bevy::{core::FixedTimestep, ecs::RunOnce, prelude::*};
use block::Block;
use field::{Field, FIELD_HEIGHT, FIELD_WIDTH};
use game_event::GameEvent;
use pieces::{ActivePiece, PiecePosition};
use position::Position;

mod block;
mod field;
mod game_event;
mod pieces;
mod position;

#[derive(Debug)]
pub(crate) enum MoveEvent {
    Move(Direction),
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub(crate) enum Direction {
    Down,
}

fn main() {
    let mut app = App::build();
    app.add_plugins(DefaultPlugins);

    app.add_resource(ClearColor(Color::BLACK))
        .add_resource(block::BlockSize(0.0))
        .add_resource(field::Field(HashMap::new()))
        .add_startup_system(setup.system())
        .add_startup_stage(
            "game_setup",
            SystemStage::serial().with_system(pieces::create_pieces.system()),
        )
        .add_stage_after(
            stage::LAST,
            "game_start",
            SystemStage::serial()
                .with_run_criteria(RunOnce::default())
                .with_system(block::init_block_size.system())
                .with_system(init_game.system()),
        )
        .add_stage_after(
            "game_start",
            "game_tick",
            SystemStage::serial()
                .with_run_criteria(FixedTimestep::step(0.5))
                .with_system(game_tick.system()),
        )
        .add_stage_after(
            stage::UPDATE,
            "transform",
            SystemStage::serial()
                .with_system(block::block_transform_system.system()),
        )
        .add_system(pieces::spawn_piece_event_handler.system())
        .add_system(movement_system.system())
        .add_system(field::lock_piece_handler.system())
        .add_event::<MoveEvent>()
        .add_event::<game_event::GameEvent>()
        .run();
}

fn setup(commands: &mut Commands) {
    commands.spawn(Camera2dBundle::default());
}

fn init_game(mut game_events: ResMut<Events<game_event::GameEvent>>) {
    game_events.send(game_event::GameEvent::SpawnNewPiece);
}

fn game_tick(mut move_events: ResMut<Events<MoveEvent>>) {
    move_events.send(MoveEvent::Move(Direction::Down));
}

pub(crate) fn movement_system(
    move_events: Res<Events<MoveEvent>>,
    mut game_events: ResMut<Events<GameEvent>>,
    mut move_events_reader: Local<EventReader<MoveEvent>>,
    field: Res<Field>,
    mut piece_query: Query<(&Children, &ActivePiece, &mut PiecePosition)>,
    mut block_query: Query<&mut Block>,
) {
    for e in move_events_reader.iter(&move_events) {
        if let Some((blocks, _, mut piece_pos)) =
            piece_query.iter_mut().next()
        {
            match e {
                MoveEvent::Move(dir) => {
                    let mut delta = Position::default();
                    match dir {
                        Direction::Down => delta.y += 1,
                    }

                    let mut is_collision = false;
                    for &b in blocks.iter() {
                        let block = block_query.get_mut(b).unwrap();
                        if collision(
                            PiecePosition(piece_pos.0 + delta),
                            block.position,
                            &field.0,
                        ) {
                            is_collision = true;
                            break;
                        }
                    }

                    if is_collision {
                        game_events.send(GameEvent::LockPiece);
                    } else {
                        piece_pos.0 += delta;
                    }
                }
            }
        }
    }
}

fn collision(
    piece_pos: PiecePosition,
    block_pos: Position,
    field: &HashMap<Position, Color>,
) -> bool {
    let pos = piece_pos.0 + block_pos;
    pos.x < 0
        || pos.x >= FIELD_WIDTH
        || pos.y >= FIELD_HEIGHT
        || field.contains_key(&pos)
}
