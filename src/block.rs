use bevy::prelude::*;

use crate::{
    field::{FIELD_HEIGHT, FIELD_WIDTH},
    pieces::PiecePosition,
    position::Position,
};

pub(crate) const BLOCK_INSET: i32 = 1;

#[derive(Clone, Copy, Debug)]
pub(crate) struct Block {
    pub position: Position,
}

impl Block {
    pub(crate) fn spawn(
        commands: &mut Commands,
        material: Handle<ColorMaterial>,
        size: f32,
        position: Position,
        color: Color,
    ) -> Entity {
        commands
            .spawn(SpriteBundle {
                sprite: Sprite::new(Vec2::new(
                    size - BLOCK_INSET as f32,
                    size - BLOCK_INSET as f32,
                )),
                transform: Transform::from_translation(Vec3::new(150.0, 0.0, 0.0)),
                material,
                ..Default::default()
            })
            .with(Block { position })
            .with(color)
            .current_entity()
            .unwrap()
    }
}

#[derive(Debug)]
pub(crate) struct BlockSize(pub f32);

pub(crate) fn init_block_size(
    windows: Res<Windows>,
    mut block_size: ResMut<BlockSize>,
) {
    let window = windows.get_primary().unwrap();
    block_size.0 = window.height() as f32 / 30.;
}

/// Transforms block coordinates in world coordinates to screen coordinates.
///
/// The world coordinate system has its origo at the upper left of the playing
/// field, the positive x-axis goes right, and the positive y-axis goes down.
///
/// The screen coordinate system has its origo at the center of the screen, the
/// positive x-axis goes right, and the positive y-axis goes up.
pub(crate) fn block_transform_system(
    block_size: Res<BlockSize>,
    mut query: Query<(Entity, &Block, &mut Transform)>,
    parent_query: Query<&Parent>,
    piece_pos_query: Query<&PiecePosition>,
) {
    for (entity, block, mut transform) in query.iter_mut() {
        let mut pos: Position = block.position;
        if let Ok(Parent(parent)) = parent_query.get(entity) {
            if let Ok(piece_pos) = piece_pos_query.get(*parent) {
                pos += piece_pos.0;
            }
        }
        let x = (pos.x as f32 - FIELD_WIDTH as f32 / 2.) * block_size.0;
        let y = -(pos.y as f32 - FIELD_HEIGHT as f32 / 2.) * block_size.0;

        transform.translation = Vec3::new(
            x,
            y,
            0.0,
        );
    }
}
