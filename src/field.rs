use std::collections::HashMap;

use bevy::prelude::*;

use crate::{
    block::Block, game_event::GameEvent, pieces::PiecePosition,
    position::Position,
};

pub(crate) const FIELD_WIDTH: i32 = 10;
pub(crate) const FIELD_HEIGHT: i32 = 4;

#[derive(Debug)]
pub(crate) struct Field(pub HashMap<Position, Color>);

pub(crate) fn lock_piece_handler(
    commands: &mut Commands,
    mut game_events_reader: Local<EventReader<GameEvent>>,
    mut game_events: ResMut<Events<GameEvent>>,
    mut field: ResMut<Field>,
    piece_query: Query<(Entity, &Children, &PiecePosition)>,
    block_query: Query<(&Block, &Color)>,
) {
    let mut spawn_new_piece = false;
    for event in game_events_reader.iter(&game_events) {
        match event {
            GameEvent::LockPiece => {
                let (entity, blocks, piece_pos) =
                    piece_query.iter().next().unwrap();
                for &b in blocks.iter() {
                    let (block, color) = block_query.get(b).unwrap();
                    field.0.insert(piece_pos.0 + block.position, *color);
                }
                spawn_new_piece = true;
                commands.despawn(entity);
            }
            _ => {}
        }
    }

    if spawn_new_piece {
        game_events.send(GameEvent::SpawnNewPiece);
    }
}
