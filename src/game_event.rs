#[derive(Debug)]
pub(crate) enum GameEvent {
    SpawnNewPiece,
    LockPiece,
}
